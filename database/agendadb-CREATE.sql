-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema agendadb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema agendadb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `agendadb` DEFAULT CHARACTER SET utf8 ;
USE `agendadb` ;

-- -----------------------------------------------------
-- Table `agendadb`.`Agenda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agendadb`.`Agenda` (
  `idAgenda` BIGINT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `senha` VARCHAR(250) NOT NULL,
  `telefone` VARCHAR(20) NULL,
  `perfil` INT NOT NULL,
  PRIMARY KEY (`idAgenda`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agendadb`.`Contato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `agendadb`.`Contato` (
  `idContato` BIGINT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `telefone` VARCHAR(20) NOT NULL,
  `email` VARCHAR(200) NULL,
  `Agenda_idAgenda` BIGINT NOT NULL,
  PRIMARY KEY (`idContato`, `Agenda_idAgenda`),
  INDEX `fk_Contato_Agenda_idx` (`Agenda_idAgenda` ASC) ,
  CONSTRAINT `fk_Contato_Agenda`
    FOREIGN KEY (`Agenda_idAgenda`)
    REFERENCES `agendadb`.`Agenda` (`idAgenda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
