class ContatoDAO:
    def __init__(self, db):
        self.db = db

    def inserir(self, contato):
        try:
            sql = "INSERT INTO Contato(nome, telefone, email, Agenda_idAgenda) " \
                  "VALUES(%s, %s, %s, %s)"
            cursor = self.db.cursor()
            cursor.execute(sql, (contato.nome, contato.telefone, contato.email, contato.id_agenda))
            self.db.commit()
            return cursor.lastrowid
        except:
            return 0

    def listar(self, id_agenda):
        sql = "SELECT * FROM Contato WHERE Agenda_idAgenda=%s"
        cursor = self.db.cursor()
        cursor.execute(sql, (id_agenda, ))
        return cursor.fetchall()

    def deletar(self, id_agenda, id_contato):
        sql = "DELETE FROM Contato WHERE Agenda_idAgenda=%s AND idContato=%s"
        cursor = self.db.cursor()
        cursor.execute(sql, (id_agenda, id_contato))
        self.db.commit()
        return cursor.rowcount










