class AgendaDAO:
    def __init__(self, db):
        self.db = db

    def inserir(self, agenda):
        try:
            sql = "INSERT INTO Agenda(nome, email, senha, telefone, perfil) " \
                  "VALUES(%s, %s, %s, %s, %s)"
            cursor = self.db.cursor()
            cursor.execute(sql, (agenda.nome, agenda.email, agenda.senha, agenda.telefone, agenda.perfil))
            self.db.commit()
            return cursor.lastrowid
        except:
            return 0

    def autenticar(self, email, senha):
        sql = "SELECT * FROM Agenda WHERE email=%s AND senha=%s"
        cursor = self.db.cursor()
        cursor.execute(sql, (email, senha))
        return cursor.fetchone()









