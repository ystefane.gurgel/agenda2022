from flask import Flask, g, request, render_template, \
    redirect, session, url_for, flash

import mysql.connector

from models.agenda import Agenda
from models.contato import Contato
from models.agendaDAO import AgendaDAO
from models.contatoDAO import ContatoDAO

app = Flask(__name__)
app.config.from_pyfile("config.py")

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = mysql.connector.connect(
            host=app.config['DBHOST'],
            user=app.config['DBUSER'],
            password=app.config['DBPASS'],
            database=app.config['DBNAME']
        )
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/cadastrar', methods=['GET', 'POST', ])
def cadastrar():
    if request.method == 'POST':
        nome = request.form['nome']
        email = request.form['email']
        telefone = request.form['telefone']
        senha = request.form['senha']
        perfil = 0
        agenda = Agenda(nome, email, senha, telefone, perfil)
        dao = AgendaDAO(get_db())
        id = dao.inserir(agenda)
        if id > 0:
            flash(f'Cadastrado com sucesso! ID = %d' % id, 'success')
        else:
            flash(f'Erro ao cadastrar agenda/usuario!', 'danger')

    return render_template('cadastrar.html')


@app.route('/cadastrar_contato', methods=['GET', 'POST', ])
def cadastrar_contato():
    if not autorizacao():
        return redirect(url_for('login'))

    if request.method == 'POST':
        nome = request.form['nome']
        email = request.form['email']
        telefone = request.form['telefone']

        contato = Contato(nome, telefone, email)
        contato.setIdAgenda(session['logado']['id'])
        dao = ContatoDAO(get_db())
        id = dao.inserir(contato)
        if id > 0:
            flash(f'Contato (id = %d) cadastrado com sucesso!' % id, 'success')
        else:
            flash(f'Erro ao cadastrar contato!', 'danger')

    return render_template('cadastrar-contato.html')

@app.route('/listar_contatos', methods=['GET',])
def listar_contatos():
    if not autorizacao():
        return redirect(url_for('login'))

    dao = ContatoDAO(get_db())
    meus_contatos = dao.listar(session['logado']['id'])
    return render_template('listar-contatos.html', contatos=meus_contatos)

@app.route('/deletar_contato/<id_contato>', methods=['GET',])
def deletar_contato(id_contato):
    if not autorizacao():
        return redirect(url_for('login'))

    dao = ContatoDAO(get_db())
    ret = dao.deletar(session['logado']['id'], id_contato)
    if ret>0:
        flash(f'Contato {id_contato} excluído!', 'success')
    else:
        flash(f'Erro ao excluído contato {id_contato}!', 'danger')
    return redirect(url_for('listar_contatos'))


@app.route('/login', methods=['GET', 'POST', ])
def login():
    if request.method == 'POST':
        email = request.form['email']
        senha = request.form['senha']
        dao = AgendaDAO(get_db())
        usuario = dao.autenticar(email, senha)
        if usuario is not None:
            session['logado'] = {'id': usuario[0],
                                 'nome': usuario[1],
                                 'email': usuario[2],
                                 'telefone': usuario[4],
                                 'perfil': usuario[5]}
            return redirect(url_for('painel'))
        else:
            flash(f'Erro ao efetuar login!', 'danger')

    return render_template('login.html')


@app.route('/logout')
def logout():
    session['logado'] = None
    session.clear()
    return redirect(url_for('index'))


@app.route('/painel')
def painel():
    if not autorizacao():
        return redirect(url_for('index'))

    return render_template('painel.html')


def autorizacao():
    if session.get('logado') is None:
        return False
    return True


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
